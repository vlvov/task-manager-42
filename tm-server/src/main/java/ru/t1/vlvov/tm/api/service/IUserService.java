package ru.t1.vlvov.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password);

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @NotNull
    UserDTO lockUserByLogin(@Nullable String id);

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String id);

    @NotNull
    @SneakyThrows
    UserDTO add(@Nullable UserDTO model);

    @NotNull
    @SneakyThrows
    Collection<UserDTO> add(@Nullable Collection<UserDTO> users);

    @NotNull
    @SneakyThrows
    Collection<UserDTO> set(@Nullable Collection<UserDTO> models);

    @SneakyThrows
    void clear();

    @NotNull
    @SneakyThrows
    List<UserDTO> findAll();

    @NotNull
    @SneakyThrows
    UserDTO remove(@Nullable UserDTO user);

    @Nullable
    @SneakyThrows
    UserDTO removeById(@Nullable String id);

    @Nullable
    @SneakyThrows
    UserDTO findOneById(@Nullable String id);

    @SneakyThrows
    boolean existsById(@Nullable String id);
}
