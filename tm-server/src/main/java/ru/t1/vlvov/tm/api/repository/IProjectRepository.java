package ru.t1.vlvov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO TM_PROJECT (ID, CREATED, NAME, DESCRIPTION, STATUS, USER_ID)"
            + "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})")
    void addProject(@NotNull ProjectDTO project);

    @Delete("DELETE FROM TM_PROJECT WHERE USER_ID = #{userId}")
    void clearProjectUserOwned(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE TM_PROJECT")
    void clearProject();

    @Select("SELECT * FROM TM_PROJECT")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<ProjectDTO> findAllProject();

    @Select("SELECT * FROM TM_PROJECT WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    ProjectDTO findProjectById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_PROJECT WHERE ID = #{id}")
    void removeProject(@NotNull ProjectDTO project);

    @Delete("DELETE FROM TM_PROJECT WHERE ID = #{id}")
    void removeProjectById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM TM_PROJECT ORDER BY NAME")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<ProjectDTO> findAllProjectSortByName(@NotNull Comparator comparator);

    @Select("SELECT * FROM TM_PROJECT WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<ProjectDTO> findAllProjectUserOwned(@Param("userId") @NotNull String userId);

    @Select("SELECT * FROM TM_PROJECT WHERE USER_ID = #{userId} AND ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    ProjectDTO findProjectByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_PROJECT WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeProjectByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM TM_PROJECT WHERE USER_ID = #{userId} ORDER BY CREATED")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<ProjectDTO> findAllProjectSortByCreated(@Param("userId") @NotNull String userId, @NotNull Comparator comparator);

    @Update("UPDATE TM_PROJECT SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status} WHERE ID = #{id}")
    void updateProject(@NotNull ProjectDTO project);

    @Update("UPDATE TM_PROJECT SET NAME = #{name}, DESCRIPTION = #{description}, STATUS = #{status} WHERE ID = #{id} AND USER_ID = #{userId}")
    void updateProjectUserOwned(@NotNull ProjectDTO project);

}
