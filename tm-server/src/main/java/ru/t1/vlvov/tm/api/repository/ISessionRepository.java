package ru.t1.vlvov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO TM_SESSION (ID, CREATED, ROLE, USER_ID)"
            + "VALUES (#{id}, #{created}, #{role}, #{userId})")
    void addSession(@NotNull SessionDTO session);

    @Delete("DELETE FROM TM_SESSION WHERE USER_ID = #{userId}")
    void clearSessionUserOwned(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE TM_SESSION")
    void clearSession();

    @Select("SELECT * FROM TM_SESSION")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @NotNull
    List<SessionDTO> findAllSession();

    @Select("SELECT * FROM TM_SESSION WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    SessionDTO findSessionById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_SESSION WHERE ID = #{id}")
    void removeSession(@NotNull SessionDTO session);

    @Delete("DELETE FROM TM_SESSION WHERE ID = #{id}")
    void removeSessionById(@Param("id") @NotNull String id);

    @Select("SELECT * FROM TM_SESSION WHERE USER_ID = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    List<SessionDTO> findAllSessionUserOwned(@Param("userId") @NotNull String userId);

    @Select("SELECT * FROM TM_SESSION WHERE USER_ID = #{userId} AND ID = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "USER_ID")
    })
    @Nullable
    SessionDTO findSessionByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_SESSION WHERE ID = #{id} AND USER_ID = #{userId}")
    void removeSessionByIdUserOwned(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

}
