package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.service.*;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.util.HashUtil;

import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @After
    public void tearDown() {
        userService.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        Assert.assertEquals(USER1.getId(), userService.findAll().get(0).getId());
    }

    @Test
    public void addList() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST);
        Assert.assertFalse(userService.findAll().isEmpty());
    }

    @Test
    public void set() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST);
        userService.set(USER_LIST2);
        Assert.assertNull(userService.findOneById(USER_LIST.get(0).getId()));
    }

    @Test
    public void clear() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST);
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.clear();
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void findAll() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST);
        Assert.assertFalse(userService.findAll().isEmpty());
    }

    @Test
    public void findOneById() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER_LIST);
        Assert.assertEquals(USER1.getId(), userService.findOneById(USER1.getId()).getId());
    }

    @Test
    public void remove() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.remove(USER1);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeById(USER1.getId());
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        Assert.assertTrue(userService.existsById(USER1.getId()));
        Assert.assertFalse(userService.existsById(USER2.getId()));
    }

    @Test
    public void createLoginPassword() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("TEST_USER1", "password1");
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals("TEST_USER1", user.getLogin());
        Assert.assertEquals(HashUtil.salt("password1", new PropertyService()), user.getPasswordHash());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void createLoginPasswordEmail() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("TEST_USER1", "password1", "test@test");
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals("TEST_USER1", user.getLogin());
        Assert.assertEquals(HashUtil.salt("password1", new PropertyService()), user.getPasswordHash());
        Assert.assertEquals("test@test", user.getEmail());
    }

    @Test
    public void createLoginPasswordRole() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("TEST_USER1", "password1", Role.USUAL);
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals("TEST_USER1", user.getLogin());
        Assert.assertEquals(HashUtil.salt("password1", new PropertyService()), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertNull(user.getEmail());
    }

    @Test
    public void findByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("TEST_USER1", "password1", "test@test");
        Assert.assertEquals(user.getId(), userService.findByLogin(user.getLogin()).getId());
    }

    @Test
    public void findByEmail() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("TEST_USER1", "password1", "test@test");
        Assert.assertEquals(user.getId(), userService.findByEmail(user.getEmail()).getId());
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        USER1.setLogin("USER1");
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userService.isLoginExist("NOT_EXIST_LOGIN"));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.findAll().isEmpty());
        USER1.setLogin("USER1");
        userService.add(USER1);
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userService.isLoginExist("NOT_EXIST_LOGIN"));
    }

    @Test
    public void removeByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.add(USER1);
        USER1.setLogin("USER1");
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeByLogin(USER1.getLogin());
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void setPassword() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("test_login", "test_password");
        userService.setPassword(user.getId(), "new_test_password");
        Assert.assertEquals(HashUtil.salt("new_test_password", new PropertyService()), userService.findOneById(user.getId()).getPasswordHash());

    }

    @Test
    public void updateUser() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("test_login", "test_password");
        userService.updateUser(user.getId(), "Ivan", "Ivanov", "Petrovich");
        Assert.assertEquals("Ivan", userService.findOneById(user.getId()).getFirstName());
        Assert.assertEquals("Ivanov", userService.findOneById(user.getId()).getLastName());
        Assert.assertEquals("Petrovich", userService.findOneById(user.getId()).getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("test_login", "test_password");
        Assert.assertFalse(userService.findOneById(user.getId()).getLocked());
        userService.lockUserByLogin(user.getLogin());
        Assert.assertTrue(userService.findOneById(user.getId()).getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertTrue(userService.findAll().isEmpty());
        @NotNull UserDTO user = userService.create("test_login", "test_password");
        Assert.assertFalse(userService.findOneById(user.getId()).getLocked());
        userService.lockUserByLogin(user.getLogin());
        Assert.assertTrue(userService.findOneById(user.getId()).getLocked());
        userService.unlockUserByLogin(user.getLogin());
        Assert.assertFalse(userService.findOneById(user.getId()).getLocked());
    }

}
