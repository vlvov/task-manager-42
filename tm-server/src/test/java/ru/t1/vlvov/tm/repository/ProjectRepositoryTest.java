package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import static ru.t1.vlvov.tm.constant.ProjectTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.clearProjectUserOwned(USER1.getId());
            repository.clearProjectUserOwned(USER2.getId());
            repository.clearProjectUserOwned(ADMIN1.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            Assert.assertNull(projectRepository.findProjectById(USER1_PROJECT1.getId()));
            projectRepository.addProject(USER1_PROJECT1);
            sqlSession.commit();
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findProjectById(USER1_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.addProject(USER1_PROJECT1);
            sqlSession.commit();
            Assert.assertEquals(USER1.getId(), projectRepository.findProjectById(USER1_PROJECT1.getId()).getUserId());
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findProjectById(USER1_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.addProject(USER1_PROJECT1);
            projectRepository.addProject(USER2_PROJECT1);
            sqlSession.commit();
            projectRepository.clearProjectUserOwned(USER1.getId());
            sqlSession.commit();
            Assert.assertNull(projectRepository.findProjectById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findProjectById(USER2_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            for (ProjectDTO project : PROJECT_LIST) {
                projectRepository.addProject(project);
            }
            sqlSession.commit();
            Assert.assertFalse(projectRepository.findAllProjectUserOwned(USER1.getId()).isEmpty());
            projectRepository.clearProjectUserOwned(USER1.getId());
            sqlSession.commit();
            Assert.assertTrue(projectRepository.findAllProjectUserOwned(USER1.getId()).isEmpty());
            Assert.assertFalse(projectRepository.findAllProject().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.addProject(USER1_PROJECT1);
            projectRepository.addProject(USER2_PROJECT1);
            sqlSession.commit();
            Assert.assertEquals(USER1_PROJECT1.getId(), projectRepository.findProjectByIdUserOwned(USER1.getId(), USER1_PROJECT1.getId()).getId());
            Assert.assertNull(projectRepository.findProjectByIdUserOwned(USER1.getId(), USER2_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.addProject(USER1_PROJECT1);
            projectRepository.addProject(USER2_PROJECT1);
            sqlSession.commit();
            projectRepository.removeProjectByIdUserOwned(USER1.getId(), USER1_PROJECT1.getId());
            sqlSession.commit();
            Assert.assertNull(projectRepository.findProjectById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findProjectById(USER2_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.addProject(USER1_PROJECT1);
            projectRepository.addProject(USER2_PROJECT1);
            sqlSession.commit();
            projectRepository.removeProjectByIdUserOwned(USER1.getId(), USER1_PROJECT1.getId());
            sqlSession.commit();
            Assert.assertNull(projectRepository.findProjectById(USER1_PROJECT1.getId()));
            Assert.assertEquals(USER2_PROJECT1.getId(), projectRepository.findProjectById(USER2_PROJECT1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.addProject(USER1_PROJECT1);
            sqlSession.commit();
            Assert.assertNotNull(projectRepository.findProjectById(USER1_PROJECT1.getId()));
            Assert.assertNull(projectRepository.findProjectById(USER2_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
