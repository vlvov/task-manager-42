package ru.t1.vlvov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.ISessionRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.service.ConnectionService;
import ru.t1.vlvov.tm.service.PropertyService;

import static ru.t1.vlvov.tm.constant.SessionTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clearSessionUserOwned(USER1.getId());
            sessionRepository.clearSessionUserOwned(USER2.getId());
            sessionRepository.clearSessionUserOwned(ADMIN1.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            System.out.println(sessionRepository);
            Assert.assertNull(sessionRepository.findSessionById(USER1_SESSION1.getId()));
            System.out.println(USER1_SESSION1);
            sessionRepository.addSession(USER1_SESSION1);
            sqlSession.commit();
            Assert.assertEquals(USER1_SESSION1.getId(), sessionRepository.findSessionById(USER1_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.addSession(USER1_SESSION1);
            sqlSession.commit();
            Assert.assertEquals(USER1.getId(), sessionRepository.findAllSession().get(0).getUserId());
            Assert.assertEquals(USER1_SESSION1.getId(), sessionRepository.findAllSession().get(0).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.addSession(USER1_SESSION1);
            sessionRepository.addSession(USER2_SESSION1);
            sqlSession.commit();
            sessionRepository.clearSessionUserOwned(USER1.getId());
            sqlSession.commit();
            Assert.assertNull(sessionRepository.findSessionById(USER1_SESSION1.getId()));
            Assert.assertEquals(USER2_SESSION1.getId(), sessionRepository.findSessionById(USER2_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            for (final SessionDTO session : SESSION_LIST) {
                sessionRepository.addSession(session);
            }
            sqlSession.commit();
            Assert.assertFalse(sessionRepository.findAllSessionUserOwned(USER1.getId()).isEmpty());
            sessionRepository.clearSessionUserOwned(USER1.getId());
            sqlSession.commit();
            Assert.assertTrue(sessionRepository.findAllSessionUserOwned(USER1.getId()).isEmpty());
            Assert.assertFalse(sessionRepository.findAllSession().isEmpty());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.addSession(USER1_SESSION1);
            sessionRepository.addSession(USER2_SESSION1);
            sqlSession.commit();
            Assert.assertEquals(USER1_SESSION1.getId(), sessionRepository.findSessionByIdUserOwned(USER1.getId(), USER1_SESSION1.getId()).getId());
            Assert.assertNull(sessionRepository.findSessionByIdUserOwned(USER1.getId(), USER2_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.addSession(USER1_SESSION1);
            sessionRepository.addSession(USER2_SESSION1);
            sqlSession.commit();
            sessionRepository.removeSessionByIdUserOwned(USER1.getId(), USER1_SESSION1.getId());
            sqlSession.commit();
            Assert.assertNull(sessionRepository.findSessionById(USER1_SESSION1.getId()));
            Assert.assertEquals(USER2_SESSION1.getId(), sessionRepository.findSessionById(USER2_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.addSession(USER1_SESSION1);
            sessionRepository.addSession(USER2_SESSION1);
            sqlSession.commit();
            sessionRepository.removeSessionByIdUserOwned(USER1.getId(), USER1_SESSION1.getId());
            sqlSession.commit();
            Assert.assertNull(sessionRepository.findSessionById(USER1_SESSION1.getId()));
            Assert.assertEquals(USER2_SESSION1.getId(), sessionRepository.findSessionById(USER2_SESSION1.getId()).getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Test
    @SneakyThrows
    public void existsByIdByUserId() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.addSession(USER1_SESSION1);
            sqlSession.commit();
            Assert.assertNotNull(sessionRepository.findSessionById(USER1_SESSION1.getId()));
            Assert.assertNull(sessionRepository.findSessionById(USER2_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
