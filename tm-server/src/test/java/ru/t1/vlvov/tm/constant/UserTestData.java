package ru.t1.vlvov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.model.UserDTO;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static UserDTO USER1 = new UserDTO();

    @NotNull
    public final static UserDTO USER2 = new UserDTO();

    @NotNull
    public final static UserDTO USER3 = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN1 = new UserDTO();

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER1, USER2, ADMIN1);

    @NotNull
    public final static List<UserDTO> USER_LIST2 = Arrays.asList(USER3, ADMIN1);

}
