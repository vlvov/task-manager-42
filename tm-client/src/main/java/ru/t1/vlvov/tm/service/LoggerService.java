package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.api.service.ILoggerService;

import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private final static String CONFIG_FILE = "logger.properties";

    @NotNull
    private final static String COMMAND = "COMMAND";

    @NotNull
    private final static String COMMAND_FILE = "./command.log";

    @NotNull
    private final static String ERROR = "ERROR";

    @NotNull
    private final static String ERROR_FILE = "./error.log";

    @NotNull
    private final static String MESSAGE = "MESSAGE";

    @NotNull
    private final static String MESSAGE_FILE = "./message.log";

    @NotNull
    private final static LogManager LOG_MANAGER = LogManager.getLogManager();

    @NotNull
    private final static Logger LOGGER_ROOT = Logger.getLogger("");

    @NotNull
    private final static Logger LOGGER_COMMAND = getLoggerCommand();

    @NotNull
    private final static Logger LOGGER_ERROR = getLoggerError();

    @NotNull
    private final static Logger LOGGER_MESSAGE = getLoggerMessage();

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    {
        init();
        registry(LOGGER_COMMAND, COMMAND_FILE, false);
        registry(LOGGER_ERROR, ERROR_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGE_FILE, true);
    }

    @NotNull
    public static Logger getLoggerCommand() {
        return Logger.getLogger(COMMAND);
    }

    @NotNull
    public static Logger getLoggerError() {
        return Logger.getLogger(ERROR);
    }

    @NotNull
    public static Logger getLoggerMessage() {
        return Logger.getLogger(MESSAGE);
    }

    private void init() {
        try {
            LOG_MANAGER.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final Exception e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + '\n';
            }
        });
        return handler;
    }

    private void registry(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final Exception e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void error(@NotNull final Exception e) {
        if (e.getMessage() == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

}
