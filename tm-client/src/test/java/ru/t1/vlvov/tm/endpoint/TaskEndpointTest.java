package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.vlvov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.request.*;
import ru.t1.vlvov.tm.dto.response.ProjectCreateResponse;
import ru.t1.vlvov.tm.dto.response.TaskCreateResponse;
import ru.t1.vlvov.tm.dto.response.TaskGetByIdResponse;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private ProjectDTO project;

    @Nullable
    private TaskDTO task;

    private String userToken;

    public TaskDTO addTask(final String name, final String description) {
        @Nullable final TaskCreateRequest requestCreateTask = new TaskCreateRequest(userToken);
        requestCreateTask.setName(name);
        requestCreateTask.setDescription(description);
        @NotNull final TaskCreateResponse responseCreateTask = taskEndpoint.createTask(requestCreateTask);
        return responseCreateTask.getTask();
    }

    public ProjectDTO addProject(final String name, final String description) {
        @Nullable final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(userToken);
        requestCreateProject.setName(name);
        requestCreateProject.setDescription(description);
        @NotNull final ProjectCreateResponse responseCreateProject = projectEndpoint.createProject(requestCreateProject);
        return responseCreateProject.getProject();
    }

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("test");
        request.setPassword("test");
        userToken = authEndpoint.login(request).getToken();
        task = addTask("name", "description");
        project = addProject("name", "description");
    }

    @After
    public void tearDown() {
        @Nullable final TaskClearRequest requestClearTask = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(requestClearTask);
        @Nullable final ProjectClearRequest requestClearProject = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClearProject);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test
    public void taskCreate() {
        @Nullable final TaskCreateRequest request = new TaskCreateRequest(userToken);
        request.setName("name");
        request.setDescription("description");
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(request);
        TaskDTO task = response.getTask();
        Assert.assertEquals("name", task.getName());
        Assert.assertEquals("description", task.getDescription());
    }

    @Test
    public void taskChangeStatusById() {
        String taskId = task.getId();
        @Nullable final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setTaskId(taskId);
        request.setStatus("In progress");
        TaskDTO task = taskEndpoint.changeTaskStatusById(request).getTask();
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void taskClear() {
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks().isEmpty());
        @Nullable final TaskClearRequest request = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(request);
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks());
    }

    @Test
    public void taskList() {
        @Nullable final TaskCreateRequest requestTaskCreate = new TaskCreateRequest(userToken);
        requestTaskCreate.setName("name2");
        requestTaskCreate.setDescription("description2");
        taskEndpoint.createTask(requestTaskCreate);
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks().isEmpty());
    }

    @Test
    public void taskRemoveById() {
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks().isEmpty());
        @Nullable final TaskRemoveByIdRequest removeByIdRequest = new TaskRemoveByIdRequest(userToken);
        removeByIdRequest.setTaskId(task.getId());
        taskEndpoint.removeTaskById(removeByIdRequest);
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks());
    }

    @Test
    public void taskShowById() {
        @Nullable final TaskGetByIdRequest requestGetById = new TaskGetByIdRequest(userToken);
        requestGetById.setTaskId(task.getId());
        final @NotNull TaskGetByIdResponse responseGetById = taskEndpoint.getTaskById(requestGetById);
        Assert.assertEquals(task.getId(), responseGetById.getTask().getId());
    }

    @Test
    public void taskUpdateById() {
        String taskId = task.getId();
        @Nullable final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setTaskId(taskId);
        request.setDescription("new description");
        request.setName("new name");
        TaskDTO task = taskEndpoint.updateTaskById(request).getTask();
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new description", task.getDescription());
    }

    @Test
    public void bindToProject() {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(project.getId());
        request.setTaskId(task.getId());
        taskEndpoint.bindTaskToProject(request);
        @Nullable final TaskListByProjectIdRequest requestListByProjectId = new TaskListByProjectIdRequest(userToken);
        requestListByProjectId.setProjectId(project.getId());
        Assert.assertEquals(task.getId(), taskEndpoint.listTaskByProjectId(requestListByProjectId).getTasks().get(0).getId());
    }

    @Test
    public void unbindFromProject() {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(project.getId());
        request.setTaskId(task.getId());
        taskEndpoint.bindTaskToProject(request);
        @Nullable final TaskListByProjectIdRequest requestListByProjectId = new TaskListByProjectIdRequest(userToken);
        requestListByProjectId.setProjectId(project.getId());
        Assert.assertEquals(task.getId(), taskEndpoint.listTaskByProjectId(requestListByProjectId).getTasks().get(0).getId());

        @Nullable final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(userToken);
        requestUnbind.setTaskId(task.getId());
        requestUnbind.setProjectId(project.getId());
        taskEndpoint.unbindTaskFromProject(requestUnbind);

        @Nullable final TaskListByProjectIdRequest requestListByProject = new TaskListByProjectIdRequest(userToken);
        requestListByProject.setProjectId(project.getId());
        Assert.assertNull(taskEndpoint.listTaskByProjectId(requestListByProject).getTasks());
    }

    @Test
    public void listByProjectId() {
        @NotNull final TaskBindToProjectRequest requestBindToProject = new TaskBindToProjectRequest(userToken);
        requestBindToProject.setProjectId(project.getId());
        requestBindToProject.setTaskId(task.getId());
        taskEndpoint.bindTaskToProject(requestBindToProject);
        @Nullable final TaskListByProjectIdRequest requestListByProject = new TaskListByProjectIdRequest(userToken);
        requestListByProject.setProjectId(project.getId());
        Assert.assertNotNull(taskEndpoint.listTaskByProjectId(requestListByProject).getTasks());
    }

}
