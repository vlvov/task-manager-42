package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IUserEndpoint;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.request.*;
import ru.t1.vlvov.tm.dto.response.UserProfileResponse;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class UserEndpointDomain {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @Nullable
    private String adminToken;

    @NotNull
    private final String userLogin = "login";

    @NotNull
    private final String userPassword = "password";

    @Nullable
    private UserDTO addUser(@NotNull final String login, @NotNull final String password, @Nullable final String email) {
        if (isUserLoginExist(login)) return null;
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        return userEndpoint.registryUser(request).getUser();
    }

    @Nullable
    private String userLogin(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return authEndpoint.login(request).getToken();
    }

    private boolean isUserLoginExist(@NotNull final String login) {
        @NotNull final UserLoginExistRequest request = new UserLoginExistRequest();
        request.setLogin(login);
        return userEndpoint.isUserLoginExist(request).isLoginExist();
    }

    @Nullable
    private UserDTO removeUser(@NotNull final String login) {
        if (!isUserLoginExist(login)) return null;
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(login);
        return userEndpoint.removeUser(request).getUser();
    }

    @Before
    public void setUp() {
        userToken = userLogin("test", "test");
        adminToken = userLogin("admin", "admin");
        removeUser(userLogin);
    }

    @After
    public void tearDown() {
        @Nullable final ProjectClearRequest requestClearProject = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClearProject);
        removeUser(userLogin);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
        @NotNull final UserLogoutRequest requestAdminLogout = new UserLogoutRequest(adminToken);
        authEndpoint.logout(requestAdminLogout);
    }

    @Test
    public void userLogin() {
        @Nullable final String userToken = userLogin("test", "test");
        Assert.assertNotNull(userToken);
        @Nullable final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName("name");
        projectCreateRequest.setDescription("description");
        projectEndpoint.createProject(projectCreateRequest);
    }

    @Test
    public void userLogout() {
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
        @Nullable final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName("name");
        projectCreateRequest.setDescription("description");
        projectEndpoint.createProject(projectCreateRequest);
    }

    @Test
    public void userRegistry() {
        @Nullable final UserDTO user = addUser(userLogin, userPassword, "email");
        Assert.assertEquals("email", user.getEmail());
        @Nullable final String userToken = userLogin(userLogin, userPassword);
        Assert.assertNotNull(userToken);
    }

    @Test(expected = RuntimeException.class)
    public void userRemove() {
        addUser(userLogin, userPassword, "email");
        @NotNull final UserRemoveRequest userRemoveRequest = new UserRemoveRequest(adminToken);
        userRemoveRequest.setLogin(userLogin);
        removeUser(userLogin);
        userLogin(userLogin, userPassword);
    }

    @Test
    public void userUpdate() {
        @NotNull final UserUpdateProfileRequest userUpdateProfileRequest = new UserUpdateProfileRequest(userToken);
        userUpdateProfileRequest.setFirstName("firstName");
        userUpdateProfileRequest.setLastName("lastName");
        userUpdateProfileRequest.setMiddleName("middleName");
        @NotNull final UserDTO user = userEndpoint.updateUserProfile(userUpdateProfileRequest).getUser();
        Assert.assertEquals("firstName", user.getFirstName());
        Assert.assertEquals("lastName", user.getLastName());
        Assert.assertEquals("middleName", user.getMiddleName());
    }

    @Test
    public void userViewProfile() {
        @NotNull final UserProfileRequest userProfileRequest = new UserProfileRequest(userToken);
        @NotNull final UserProfileResponse userProfileResponse = userEndpoint.profile(userProfileRequest);
        Assert.assertEquals("test", userProfileResponse.getUser().getLogin());
    }

}
